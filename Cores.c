#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <locale.h>



enum Cor
{

    BLACK, BLUE, GREEN, CYAN, RED, MAGENTA, BROWN,
    LIGHT_GRAY, DARK_GRAY, LIGHT_BLUE, LIGHT_GREEN, LIGHT_CYAN,
    LIGHT_RED, LIGHT_MAGENTA, YELLOW, WHITE

};
/*------------------------------------------------------------------------*/
void corTexto (enum Cor iColor)
{

    HANDLE hl = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO bufferInfo;
    BOOL b = GetConsoleScreenBufferInfo(hl, &bufferInfo);
    bufferInfo.wAttributes &= 0x000F2;
    SetConsoleTextAttribute(hl, bufferInfo.wAttributes |= (iColor));

}
/*-------------------------------------------------------------------------*/
void corFundo (enum Cor iColor)
{

    HANDLE hl = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO bufferInfo;
    BOOL b = GetConsoleScreenBufferInfo(hl, &bufferInfo);
    bufferInfo.wAttributes &= 0x000F;
    SetConsoleTextAttribute(hl, bufferInfo.wAttributes |= (iColor << 4));

}
/*--------------------------------------------------------------------------*/


int main(void)
{

    int fundo, texto;

    enum Cor c[16] = {BLACK, BLUE, GREEN, CYAN, RED, MAGENTA, BROWN,
                      LIGHT_GRAY, DARK_GRAY, LIGHT_BLUE, LIGHT_GREEN, LIGHT_CYAN,
                      LIGHT_RED, LIGHT_MAGENTA, YELLOW, WHITE
                     };

    setlocale(LC_ALL, "Portuguese");
    SetConsoleTitle("Cores com FUNCOES");

    printf("\t\t\tColors Table\n");
    printf("\t0 - Preto\t\t8 - Cinza Escuro\n");
    printf("\t1 - Azul\t\t9 - Azul Claro\n");
    printf("\t2 - Verde\t\t10 - Verde Claro\n");
    printf("\t3 - Ciano\t\t11 - Ciano Claro\n");
    printf("\t4 - Vermelho\t\t12 - Vermelho Claro\n");
    printf("\t5 - Magenta\t\t13 - Magenta Claro\n");
    printf("\t6 - Marrom\t\t14 - Amarelo\n");
    printf("\t7 - Cinza Claro\t\t15 - Branco\n");

    printf("Cor de fundo: ");
    scanf("%d", &fundo);

    printf("Cor Texto: ");
    scanf("%d", &texto);

    system("cls");

    corFundo(c[fundo]);
    corTexto(c[texto]);

    printf("As cores foram alteradas.");
}



