#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "batalha.h"

;int main(){
    srand(time(NULL));
    struct Atributo guerreiro;
    guerreiro.ataque = 120;
    guerreiro.defesa = 90;
    guerreiro.vida = 300;

    int resp, resp2, resp3, respb;
    int dano;
    int retorno = 1;

    do{
    printf("\nGuerreiro, qual inimigo deseja enfrentar?");
    printf("\n[1] Ogro, [2] Goblin, [3] Barbaro, [4] Barbarian boss");
    scanf("%d", &resp);

    switch(resp){
        case 1:
            printf("%s\n", "Seus Atributos:");
            printf("%s", "\nAtaque:");
            printf("%d", guerreiro.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", guerreiro.defesa);
            printf("%s", "\nVida:");
            printf("%d", guerreiro.vida);

            srand(time(NULL));
            struct Atributo ogro;
            ogro.ataque = 50 + rand() %50;
            ogro.defesa = 50 + rand() %30;
            ogro.vida = 100 + rand() %100;
            //Realizando ataque
            printf("%s", "\nAtributos iniciais do inimigo:");
            printf("%s", "\nAtaque:");
            printf("%d", ogro.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", ogro.defesa);
            printf("%s", "\nVida:");
            printf("%d", ogro.vida);
            do{
            dano = (guerreiro.ataque - ogro.defesa);
            srand(time(NULL));
            int critical = rand() %10;
            //printf("\n%S", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Causou dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = guerreiro.ataque/2;
                int quebradef = rand() %controle;
                ogro.defesa = ogro.defesa - quebradef;
                printf("%s", "\nDefesa do inimigo diminuida para");
                printf("%d", ogro.defesa);
            }
            printf("%s", "\nDano = ");
            printf("%d", dano);
            ogro.vida = (ogro.vida - dano);
            printf("%s", "\nVida restante do inimigo:");
            printf("%d", ogro.vida);

            if(ogro.vida <= 0){
                printf("%s", "\n O inimigo foi destruido");
                resp2 = 1;
                resp3 = 2;
            }else{
            //Realizando defesa
            dano = (ogro.ataque - guerreiro.defesa);
            srand(time(NULL));
            critical = rand() %10;
            //printf("%s\n", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Sofreu dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = ogro.ataque/2;
                int quebradef = rand() %controle;
                guerreiro.defesa = guerreiro.defesa - quebradef;
                printf("%s", "\nSua defesa foi diminuida para ");
                printf("%d", guerreiro.defesa);
            }
            printf("%s", "\nDano sofrido = ");
            printf("%d", dano);
            guerreiro.vida = (guerreiro.vida - dano);
            printf("\n%s", "Sua vida restante:");
            printf("%d", guerreiro.vida);
             if(guerreiro.vida <= 0){
                printf("%s", "\n Game Over: Voce foi Destru�do");
                resp2 = 2;
                resp3 = 2;
            }else{
            //
            printf("\n%s", "Guerreiro, deseja continuar a batalha?");
            printf("\n[1] continuar, [2] fugir");
            scanf("%d", &respb);
            if(respb == 1){
                    resp3 = 1;
            }else if(respb == 2){
                    resp3 = 0;
                    resp2 = 1;
            }
            }
            }
            }while(resp3 == 1);
            break;
        case 2:
            guerreiro.ataque = (guerreiro.ataque + 30);
            guerreiro.defesa = (guerreiro.defesa + 30);

            printf("%s\n", "Seus Atributos:");
            printf("%s", "\nAtaque:");
            printf("%d", guerreiro.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", guerreiro.defesa);
            printf("%s", "\nVida:");
            printf("%d", guerreiro.vida);

            srand(time(NULL));
            struct Atributo goblin;
            goblin.ataque = 100 + rand() %50;
            goblin.defesa = 100 + rand() %30;
            goblin.vida = 150 + rand() %100;
            //Realizando ataque
            printf("%s", "\nAtributos iniciais do inimigo:");
            printf("%s", "\nAtaque:");
            printf("%d", goblin.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", goblin.defesa);
            printf("%s", "\nVida:");
            printf("%d", goblin.vida);
            do{
            dano = (guerreiro.ataque - goblin.defesa);
            srand(time(NULL));
            int critical = rand() %10;
            //printf("\n%S", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Causou dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = guerreiro.ataque/2;
                int quebradef = rand() %controle;
                goblin.defesa = goblin.defesa - quebradef;
                printf("%s", "\nDefesa do inimigo diminuida para");
                printf("%d", goblin.defesa);
            }
            printf("%s", "\nDano = ");
            printf("%d", dano);
            goblin.vida = (goblin.vida - dano);
            printf("%s", "\nVida restante do inimigo:");
            printf("%d", goblin.vida);

            if(goblin.vida <= 0){
                printf("%s", "\n O inimigo foi destruido");
                resp2 = 1;
                resp3 = 2;
            }else{
            //Realizando defesa
            dano = (goblin.ataque - guerreiro.defesa);
            srand(time(NULL));
            critical = rand() %10;
            //printf("%s\n", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Sofreu dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = ogro.ataque/2;
                int quebradef = rand() %controle;
                guerreiro.defesa = guerreiro.defesa - quebradef;
                printf("%s", "\nSua defesa foi diminuida para ");
                printf("%d", guerreiro.defesa);
            }
            printf("%s", "\nDano sofrido = ");
            printf("%d", dano);
            guerreiro.vida = (guerreiro.vida - dano);
            printf("\n%s", "Sua vida restante:");
            printf("%d", guerreiro.vida);
             if(guerreiro.vida <= 0){
                printf("%s", "\n Game Over: Voce foi Destru�do");
                resp2 = 2;
                resp3 = 2;
            }else{
            //
            printf("\n%s", "Guerreiro, deseja continuar a batalha?");
            printf("\n[1] continuar, [2] fugir");
            scanf("%d", &respb);
            if(respb == 1){
                    resp3 = 1;
            }else if(respb == 2){
                    resp3 = 0;
                    resp2 = 1;
            }
            }
            }
            }while(resp3 == 1);

            break;
        case 3:
            guerreiro.ataque = (guerreiro.ataque + 50);
            guerreiro.defesa = (guerreiro.defesa + 50);

            printf("%s\n", "Seus Atributos:");
            printf("%s", "\nAtaque:");
            printf("%d", guerreiro.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", guerreiro.defesa);
            printf("%s", "\nVida:");
            printf("%d", guerreiro.vida);

            srand(time(NULL));
            struct Atributo barbaro;
            barbaro.ataque = 150 + rand() %50;
            barbaro.defesa = 150 + rand() %30;
            barbaro.vida = 200 + rand() %100;
            //Realizando ataque
            printf("%s", "\nAtributos iniciais do inimigo:");
            printf("%s", "\nAtaque:");
            printf("%d", barbaro.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", barbaro.defesa);
            printf("%s", "\nVida:");
            printf("%d", barbaro.vida);
            do{
            dano = (guerreiro.ataque - barbaro.defesa);
            srand(time(NULL));
            int critical = rand() %10;
            //printf("\n%S", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Causou dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = guerreiro.ataque/2;
                int quebradef = rand() %controle;
                barbaro.defesa = barbaro.defesa - quebradef;
                printf("%s", "\nDefesa do inimigo diminuida para");
                printf("%d", barbaro.defesa);
            }
            printf("%s", "\nDano = ");
            printf("%d", dano);
            barbaro.vida = (barbaro.vida - dano);
            printf("%s", "\nVida restante do inimigo:");
            printf("%d", barbaro.vida);

            if(barbaro.vida <= 0){
                printf("%s", "\n O inimigo foi destruido");
                resp2 = 1;
                resp3 = 2;
            }else{
            //Realizando defesa
            dano = (barbaro.ataque - guerreiro.defesa);
            srand(time(NULL));
            critical = rand() %10;
            //printf("%s\n", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Sofreu dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = ogro.ataque/2;
                int quebradef = rand() %controle;
                guerreiro.defesa = guerreiro.defesa - quebradef;
                printf("%s", "\nSua defesa foi diminuida para ");
                printf("%d", guerreiro.defesa);
            }
            printf("%s", "\nDano sofrido = ");
            printf("%d", dano);
            guerreiro.vida = (guerreiro.vida - dano);
            printf("\n%s", "Sua vida restante:");
            printf("%d", guerreiro.vida);
             if(guerreiro.vida <= 0){
                printf("%s", "\n Game Over: Voce foi Destru�do");
                resp2 = 2;
                resp3 = 2;
            }else{
            //
            printf("\n%s", "Guerreiro, deseja continuar a batalha?");
            printf("\n[1] continuar, [2] fugir");
            scanf("%d", &respb);
            if(respb == 1){
                    resp3 = 1;
            }else if(respb == 2){
                    resp3 = 0;
                    resp2 = 1;
            }
            }
            }
            }while(resp3 == 1);

            break;

        case 4:
            guerreiro.ataque = (guerreiro.ataque + 70);
            guerreiro.defesa = (guerreiro.defesa + 70);

            printf("%s\n", "Seus Atributos:");
            printf("%s", "\nAtaque:");
            printf("%d", guerreiro.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", guerreiro.defesa);
            printf("%s", "\nVida:");
            printf("%d", guerreiro.vida);


            srand(time(NULL));
            struct Atributo barbarian;
            barbarian.ataque = 200 + rand() %50;
            barbarian.defesa = 200 + rand() %30;
            barbarian.vida = 250 + rand() %100;

            //Realizando ataque
            printf("%s", "\nAtributos iniciais do inimigo:");
            printf("%s", "\nAtaque:");
            printf("%d", barbarian.ataque);
            printf("%s", "\nDefesa:");
            printf("%d", barbarian.defesa);
            printf("%s", "\nVida:");
            printf("%d", barbarian.vida);
            do{
            dano = (guerreiro.ataque - barbarian.defesa);
            srand(time(NULL));
            int critical = rand() %10;
            //printf("\n%S", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Causou dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = guerreiro.ataque/2;
                int quebradef = rand() %controle;
                barbarian.defesa = barbarian.defesa - quebradef;
                printf("%s", "\nDefesa do inimigo diminuida para");
                printf("%d", barbarian.defesa);
            }
            printf("%s", "\nDano = ");
            printf("%d", dano);
            barbarian.vida = (barbarian.vida - dano);
            printf("%s", "\nVida restante do inimigo:");
            printf("%d", barbarian.vida);

            if(barbarian.vida <= 0){
                printf("%s", "\n O inimigo foi destruido");
                resp2 = 1;
                resp3 = 2;
            }else{
            //Realizando defesa
            dano = (barbarian.ataque - guerreiro.defesa);
            srand(time(NULL));
            critical = rand() %10;
            //printf("%s\n", "Sorteio:");
            //printf("%d", critical);
            if(critical == 7){
                printf("%s", "Sofreu dano critico(+50)");
                dano = dano + 50;
            }
            if(dano <= 0){
                printf("%s", "\nMiss");
                dano = 0;
                int controle = barbarian.ataque/2;
                int quebradef = rand() %controle;
                guerreiro.defesa = guerreiro.defesa - quebradef;
                printf("%s", "\nSua defesa foi diminuida para ");
                printf("%d", guerreiro.defesa);
            }
            printf("%s", "\nDano sofrido = ");
            printf("%d", dano);
            guerreiro.vida = (guerreiro.vida - dano);
            printf("\n%s", "Sua vida restante:");
            printf("%d", guerreiro.vida);
             if(guerreiro.vida <= 0){
                printf("%s", "\n Game Over: Voce foi Destru�do");
                resp2 = 2;
                resp3 = 2;
            }else{
            //
            printf("\n%s", "Guerreiro, deseja continuar a batalha?");
            printf("\n[1] continuar, [2] fugir");
            scanf("%d", &respb);
            if(respb == 1){
                    resp3 = 1;
            }else if(respb == 2){
                    resp3 = 0;
                    resp2 = 1;
            }
            }
            }
            }while(resp3 == 1);

            break;
        default:
            printf("Op��o Inv�lida!!");
            resp2 = 1;
            break;
            }
    }while(resp2 == 1);
    //printf("%d", at);
    getch();
}
