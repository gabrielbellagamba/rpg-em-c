void salvarProgresso (struct Atributo guerreiro, int posicao)
{
    FILE *file;
    file = fopen("dados.txt","w");
    fprintf(file,"%i %i %i %d",guerreiro.ataque, guerreiro.defesa,guerreiro.vida, posicao);
    fclose(file);
}

void carregarArquivos(struct Atributo *guerreiro)
{
    FILE *file;
    file = fopen("dados.txt", "r");

    if (file == NULL)
    {
        printf("Erro ao carregar os dados.");
    }
    else
    {
        fscanf(file,"%i %i %i", &(*guerreiro).ataque,&(*guerreiro).defesa, &(*guerreiro).vida);
        printf("Dados carregados.");
    }
    fclose(file);
}
