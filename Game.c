// A C Program to demonstrate adjacency list
// representation of graphs
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <gprolog.h>
#include "GraphCode.h"
#define SIZE 200


static int
Main_Wrapper(int argc, char *argv[])
{
    int func;
  PlTerm arg[10];
  int number;
  char *sol[100];
  int i, nb_sol = 0;
  PlBool res;

  Pl_Start_Prolog(argc, argv);

  func = Pl_Find_Atom("contem");
  for (;;)
    {
      printf("\nInforme a posicao que deseja consultar ou digite 99 para sair\n");
      fflush(stdout);
      scanf("%d", &number);

      if (number == 99){
	  Pl_Stop_Prolog();
	return 0;
 	break;

	}

      Pl_Query_Begin(PL_TRUE);

      arg[0] = Pl_Mk_Integer(number);
      arg[1] = Pl_Mk_Variable();
      nb_sol = 0;
      res = Pl_Query_Call(func, 2, arg);
      while (res)
 {
   sol[nb_sol++] = Pl_Rd_String(arg[1]);
   res = Pl_Query_Next_Solution();
 }
      Pl_Query_End(PL_RECOVER);

      for (i = 0; i < nb_sol; i++)
	 printf("  O local informado %d contem: %s\n",number, sol[i]);
    }

  Pl_Stop_Prolog();
  return 0;

}

int consultaOraculo(int argc, char *argv[]){
	return Main_Wrapper(argc,argv);
}


//Gabriel Bellagamba Start
typedef struct
{
    int v[8];
    int vTemp[8];
    int vFinal[4];
} CHAR_ARRAY;


char *def_Name_Levels(int value)
{
    int i = 0;

    char *levelName[] =
        {
            "1", "2", "Casa de pedra interior", "4", "5", "Casa do pantano quarto", "7",
            "Casa de pedra exterior", "9", "10", "Casa do pantano sala", "12",
            "Planice de pedra","14", "Rocha excalibur",
             "Casa do pantano exterior", "Pantano", "Inicio", "19", "Ponte de pedra quebrada",
             "21", "22", "Floresta negra", "Choupana na floresta exterior",
             "Choupana na floresta interior",
              "Pantano morbido", "Estrada sombria", "Estrada de terra pt-1", "29", "30",
              "Pequeno vilarejo", "32", "Estrada de terra pt-2", "34", "35",
              "Trilha dos goblins", "37", "Portal norte", "Salao refeicoes",
              "Corredor nordeste", "Ponte dos goblins", "Portal oeste", "Corredor oeste",
               "44", "Grande passagem salao principal", "46", "47", "Corredor sudoeste", "Biblioteca",
               "Salao principal", "51", "52", "Quarto do barbarian", "54", "55"
        };

    static char level[SIZE];

    for (i = 0; i < 55; i++)
    {
        if (i == value)
        {
            strcpy(level, levelName[i - 1]);
        }
    }

    return level;
}
//Gabriel Bellagamba End
//Gabriel Bellagamba Start
char *def_Content_Levels(int value)
{
    int i = 0;
    //* = life
    //% = key
    //@ = defence
    //$ = damage

    char *levelContent[] =
        {
            "1", "2", "*", "4", "5", "@", "7", "%", "9", "10", "*", "12", "@", "14", "&",
            "*", "$", "*", "19", "*", "21", "22", "*", "%", "$",
            "@", " ", "$", "29", "30", "$", "*", "$", "34", "35",
            " ", "37", " ", "*", "%", "$", "@", " ",
            "44","%", "46", "47", "$", "@", "*", "51", "52", "!", "54", "55"};


    static char content[SIZE];

    for (i = 0; i < 55; i++)
    {
        if (i == value)
        {
            strcpy(content, levelContent[i - 1]);
        }
    }

    return content;
}

char *def_Enemy_Levels(int value)
{
    int i = 0;

    char *enemyContent[] =
        {
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", ")", "14", "&",
            "16", "!", "18", "19", ")", "21", "22", "!", "24", "25",
            "!", "27", "28", "29", "30", "31", "32", "33", "34", "35",
            "36", "37", ")", "39", "40", "#", ")", "43",
            "44","45", "46", "47", "48", "49", ")", "51", "52", "&", "54", "55"};


    static char content[SIZE];

    for (i = 0; i < 55; i++)
    {
        if (i == value)
        {
            strcpy(content, enemyContent[i - 1]);
        }
    }

    return content;
}

CHAR_ARRAY returnArray(CHAR_ARRAY array_in, int size, struct Graph *graph, int node)
{
    int i = 0;
    int j = 0;
    int l = 0;
    int k = 0;
    int a[20] = {0, 0, 0, 0, 0, 0, 0, 0};
    CHAR_ARRAY returned;

    struct AdjListNode *pCrawl = graph->array[node].head;

    while (pCrawl)
    {
        if (i < size)
        {
            a[i] = pCrawl->dest;
            pCrawl = pCrawl->next;
            i++;
        }
    }
    //Stack/2 Start
    for (i = 0; i < size; i++)
    {
        for (j = i + 1; j < size;)
        {
            if (a[j] == a[i])
            {
                for (k = j; k < size; k++)
                {
                    a[k] = a[k + 1];
                }
                size--;
            }
            else
            {
                j++;
            }
        }
    }


    for (i = 0; i < size; i++)
    {

        returned.vFinal[i] = a[i];
    }
    return returned;
}

void addEdge(struct Graph *graph, int src, int dest, char value[SIZE])
{
    struct AdjListNode *newNode = newAdjListNode(dest, value);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
    newNode = newAdjListNode(src, value);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

void printGraph(struct Graph *graph)
{
    int v;
    for (v = 0; v < graph->V; ++v)
    {
        struct AdjListNode *pCrawl = graph->array[v].head;
        printf("\n Adjacency list of vertex %d head\n", v);
        while (pCrawl)
        {
            printf("-> %d", pCrawl->dest);
            printf(" value = %s\n", pCrawl->value);

            pCrawl = pCrawl->next;
        }
        printf("\n");
    }
}

void printNodeAdjs(struct Graph *graph, int node)
{
    struct AdjListNode *pCrawl = graph->array[node].head;
    printf("\n Adjacency list of vertex %d\n head ", node);
    while (pCrawl)
    {
        printf("-> %d", pCrawl->dest);
        printf(" value = %s", pCrawl->value);

        pCrawl = pCrawl->next;
    }
    printf("\n");
}

int getNextNodeValue(struct Graph *graph, int node, int nodeAdj){
    struct AdjListNode *pCrawl = graph->array[node].head;

    for (int i = 1; i <= nodeAdj; i++)
    {
        pCrawl = pCrawl->next;
    }

    int value = pCrawl->dest;

    return value;
}

int verify_key(){
    int i;
    int a = 0;
    char *inventory[] ={"1", "2", "3", "4", "5"};

    for (i=0; i<5; i++){
        if (strcmp(inventory[i],"7")== 0){
            a = 1;
            return a;
        }
    }
    return a;
}

void getItem_On_Level (int pos){


    if (strcmp(def_Content_Levels(pos-1),"*") == 1){
//        printf("\n\n++++ Voce encontrou uma pocao de vida e teve sua vida aumentada em 10 pts ++++\n\n");
    }
     else if (strcmp(def_Content_Levels(pos-1),"%") == 1){
//        printf("\n\n++++ Voce encontrou uma chave de sala secreta ++++\n\n");
    }
     else if (strcmp(def_Content_Levels(pos-1),"@") == 1){
//        printf("\n\n++++ Voce encontrou um escudo e teve sua defesa aumentada em 10 pts ++++\n\n");
    }
     else if (strcmp(def_Content_Levels(pos-1),"$") == 1){
//        printf("\n\n++++ Voce encontrou uma espada e teve seu dano aumentado em 10 pts ++++\n\n");
    } else {
//        printf("\n\nA sala nao possui itens\n\n");
    }
}

void getEnemy_On_Level (int pos){

    if (strcmp(def_Enemy_Levels(pos),"#") == 0){
//        printf("\n\nVoce encontrou um Goblin\n\n");
    }
     else if (strcmp(def_Enemy_Levels(pos),"!") == 0){
//        printf("\n\nVoce encontrou um Ogro\n\n");
    }
     else if (strcmp(def_Enemy_Levels(pos),")") == 0){
//        printf("\n\nVoce encontrou um Barbaro\n\n");
    }
     else if (strcmp(def_Enemy_Levels(pos),"&") == 0){
//        printf("\n\nVoce encontrou um tigre barbarian\n\n");
    } else {
//        printf("\n\nA sala nao possui inimigos\n\n");
    }
}

int fileRead (char fileName[30]){

	FILE *infile;

	infile = fopen (fileName, "r");
	if (infile == NULL)
	{
		fprintf(stderr, "\nError opening file\n");
		exit (1);
	}
	char frase [500];
	while(fgets(frase, 500, infile)!= NULL){
		printf ("%s", frase);
	}
	fclose (infile);
	return 0;
}
char* appendToName(char fileName[SIZE]) {

    char str[80];
    strcpy(str,fileName);
    strcat(str, ".txt");

    static char nameAndExt[SIZE];

    strcpy(nameAndExt,str);

    return nameAndExt;

}

void mapa(){
printf("\t\t\t\t\t\t###==========Aqui está o mapa========###\n\n\n");
printf("\t\t\t\t\t\t [3-Casa de Pedra] \t\t\n");

printf("\n\n\n");
printf("[6-Casa do pantano] \t\t\t\t [8-Casa de pedra] \t\t\n");
printf("[Interno quarto\t  ] \t\t\t\t [Externo\t ] \t\t\n");
printf("\n\n\n");
printf("[11-Casa do pantano] \t\t\t\t [13-Planice de pedra] \t\t\t\t\t\t [15-Rocha Excalibur]\n");
printf("[Interno sala\t   ] \t\t\t\t [\t\t     ] \t\t\t\t\t\t [\t\t    ]\n");
printf("\n\n\n");
printf("[16-Casa do pantano] \t [17-Pantano] \t\t [18-Inicio] \t\t\t\t\t\t\t [20-Ponte de pedra quebrada]\n");
printf("[Externo\t   ] \t [\t    ] \t\t [\t   ] \t\t\t\t\t\t\t [\t\t\t    ]\n");
printf("\n\n\n");
printf("\t\t\t\t\t\t [23-Floresta Negra] \t\t [24-Choupana na floresta] \t [25-Choupana na floresta]\n");
printf("\t\t\t\t\t\t [\t\t   ] \t\t [Externo\t\t ] \t [Interno\t\t ]\n");
printf("\n\n\n");
printf("[26-Pantano morbido] \t [27-Estrada sombria] \t [28-Estrada de terra]\n");
printf("[\t\t   ] \t [\t\t    ] \t [\t\t     ]\n");
printf("\n\n\n");
printf("[31-Pequeno vilarejo] \t\t\t\t [33-Estrada de terra]\n");
printf("[\t\t    ] \t\t\t\t [\t\t     ]\n");
printf("\n\n\n");
printf("[36-Trilha do goblins]\t\t\t\t [38-Portal Norte] \t\t [39-Salao de refeicoes] \t [40-Corredor nordeste]\n");
printf("[\t\t     ]\t\t\t\t [Forte Barabaro ] \t\t [\t\t       ] \t [\t\t    ]\n");
printf("\n\n\n");
printf("[41-Ponte dos goblins] \t [42-Portal oeste] \t [43-Corredor oeste] \t\t\t\t\t\t [45-Grande passagem salao principal]\n");
printf("[\t\t     ] \t [\t\t ] \t [\t\t   ] \t\t\t\t\t\t [\t\t\t\t    ]\n");
printf("\n\n\n");
printf("\t\t\t\t\t\t [48-Corredor sudoeste] \t [49-Biblioteca] \t\t [50-Salao principal]\n");
printf("\t\t\t\t\t\t [\t\t      ]    \t [\t       ] \t\t [\t\t    ]\n");
printf("\n\n\n");
printf("\t\t\t\t\t\t [53-Quarto barbarian] \t\t\n");
printf("\t\t\t\t\t\t [\t\t     ] \t\t\n");
printf("\n\n\n");
}



void jogar(int argc, char *argv[]){

    int V = 41;
    struct Graph *graph = createGraph(V);
    addEdge(graph, 3, 8, "CasaPedra2");
    addEdge(graph, 6, 11, "CasaPantanoSala");
    addEdge(graph, 8, 3, "CasaPedra1");
    addEdge(graph, 8, 13, "PlanicePedra");
    addEdge(graph, 11, 6, "CasaPantanoQuarto");
    addEdge(graph, 11, 16, "CasaPantano");
    addEdge(graph, 13, 8, "CasaPedra");
    addEdge(graph, 13, 18, "Inicio");
    addEdge(graph, 15, 20, "PontePedraQuebrada");
    addEdge(graph, 16, 11, "CasaPantanoSala");
    addEdge(graph, 16, 17, "Pantano");
    addEdge(graph, 17, 18, "Inicio");
    addEdge(graph, 17, 16, "CasaPantano");
    addEdge(graph, 18, 13, "planicePedra");
    addEdge(graph, 18, 17, "pantano");
    addEdge(graph, 18, 23, "florestaNegra");
    addEdge(graph, 20, 15, "rochaExcalibur");
    addEdge(graph, 20, 25, "PonteDePedraQuebrada");
    addEdge(graph, 23, 18, "inicio");
    addEdge(graph, 23, 24, "choupanaNaFlorestaExt");
    addEdge(graph, 23, 28, "estradaDeTerra");
    addEdge(graph, 24, 25, "choupanaNaFlorestaInt");
    addEdge(graph, 24, 23, "florestaNegra");
    addEdge(graph, 25, 20, "PonteDePedraQuebrada");
    addEdge(graph, 25, 24, "choupanaNaFlorestaExt");
    addEdge(graph, 26, 27, "EstradaSombria");
    addEdge(graph, 26, 31, "PequenoVilarejo");
    addEdge(graph, 27, 28, "EstradaDeTerra1");
    addEdge(graph, 27, 26, "pantanoMorbido");
    addEdge(graph, 28, 23, "FlorestaNegra");
    addEdge(graph, 28, 33, "EstradaDeTerra2");
    addEdge(graph, 28, 27, "EstradaSombria");
    addEdge(graph, 31, 26, "PantanoMorbido");
    addEdge(graph, 31, 36, "TrilhaDosGoblins");
    addEdge(graph, 33, 28, "EstradaDeTerra1");
    addEdge(graph, 33, 38, "PortalNorte");
    addEdge(graph, 36, 31, "PeqVilarejo");
    addEdge(graph, 36, 41, "PonteDosGoblins");
    addEdge(graph, 38, 33, "EstradaDeTerra");
    addEdge(graph, 38, 39, "SalaoRefeicoes");
    addEdge(graph, 38, 43, "PortalOeste");
    addEdge(graph, 39, 40, "CorredorNordeste");
    addEdge(graph, 39, 38, "PortalNorte");
    addEdge(graph, 40, 45, "GrandePassagem");
    addEdge(graph, 40, 39, "SalaoRefeicoes");
    int option;
    CHAR_ARRAY array = {0, 0, 0, 0, 0, 0, 0, 0};
    int arrayCount = 8;
    int value = 18;

    do
    {

        CHAR_ARRAY returnedArray = returnArray(array, arrayCount, graph, value);
        printf("SELECIONE A SUA OPCAO\n\n");
        for (int i = 0; i <= 3; i++)
        {
            if (returnedArray.vFinal[i] != 0 && returnedArray.vFinal[i] < 56)
            {
                printf("OPCAO [%d]=", i + 1);
                printf("[%s]\t", def_Name_Levels(returnedArray.vFinal[i]));
            }
        }
        printf("\n\n");
        printf("OPCAO [7]=");
        printf("[MAPA]\t");

        printf("\tOPCAO [8]=");
        printf("[CONSULTAR]\t");

        printf("OPCAO [9]=");
        printf("[SAIR]\t");

        printf("\n\n");

        for (int i = 0; i <= 3; i++)
        {
            if (returnedArray.vFinal[i] != 0 && returnedArray.vFinal[i] < 56)
            {


                if (returnedArray.vFinal[i]== 15){
                        printf("\n\n===Procurando a chave...");
                    if (verify_key()==1){
                            printf("\nvoce possui a chave, parabens!");
                        returnedArray.vFinal[i]= 15;
                    }else {
                        printf("\n\nVoce nao possui a chave ainda\n\n");
                        returnedArray.vFinal[i]= 20;
                    }
                }
            }
        }



        scanf("%d", &option);

        switch (option)
        {

        case 1:
            printf("\n\n=====VOCE ESTA #%s=====\n\n", def_Name_Levels(returnedArray.vFinal[0]));
            printf("VOCE SELECIONOU [%d]=", returnedArray.vFinal[0]);
            fileRead(appendToName(def_Name_Levels(returnedArray.vFinal[0])));
            getItem_On_Level((int)returnedArray.vFinal[0]);
            getEnemy_On_Level((int)returnedArray.vFinal[0]);
            verify_key();
            value = returnedArray.vFinal[0];
            break;

        case 2:
            printf("\n\n=====VOCE ESTA #%s=====\n\n", def_Name_Levels(returnedArray.vFinal[1]));
            printf("VOCE SELECIONOU [%d]=", returnedArray.vFinal[1]);
            fileRead(appendToName(def_Name_Levels(returnedArray.vFinal[1])));
            getItem_On_Level((int)returnedArray.vFinal[1]);
            getEnemy_On_Level((int)returnedArray.vFinal[1]);
            value = returnedArray.vFinal[1];
            break;
        case 3:
            printf("\n\n=====VOCE ESTA #%s=====\n\n", def_Name_Levels(returnedArray.vFinal[2]));
            printf("VOCE SELECIONOU [%d]=", returnedArray.vFinal[2]);
            fileRead(appendToName(def_Name_Levels(returnedArray.vFinal[2])));
            getItem_On_Level((int)returnedArray.vFinal[2]);
            getEnemy_On_Level((int)returnedArray.vFinal[2]);
            value = returnedArray.vFinal[2];
            break;
        case 4:
            printf("\n\n=====VOCE ESTA #%s=====\n\n", def_Name_Levels(returnedArray.vFinal[3]));
            printf("VOCE SELECIONOU [%d]=", returnedArray.vFinal[3]);
            fileRead(appendToName(def_Name_Levels(returnedArray.vFinal[3])));
            getItem_On_Level((int)returnedArray.vFinal[3]);
            getEnemy_On_Level((int)returnedArray.vFinal[3]);
            value = returnedArray.vFinal[3];
            break;

        case 7:
            mapa();
            break;

         case 8:
                printf("\n\n=====VOCE ESCOLHEU #CONSULTA=====\n\n");
                printf("\n\nVoce selecionou [%d]\n\n",option);
		consultaOraculo(argc, argv);
            break;

        case 9:
            printf("\n\n===VOCE ESCOLHEU SAIR===\n\n");
            printf("Deseja  realmente sair ?\n");
            printf("[1]-SIM\t[2]-CANCELAR");
            int opt2;
            scanf("%d", &opt2);

            switch (opt2){

            case 1:
                printf("\n\nVoce escolheu [1]\n\n");
                printf("Salvando seu progresso e saindo\n");
                option = 9;
            break;

            case 2:
                printf("\n\nVoce escolheu [3]\n\n");
                printf("Retornando ao jogo\n");
                option = 8;
            break;

            default:
                printf("opcao invalida!");
                break;
            }

        break;

        default:
            printf("\nopt Invalida\n");
        }

    } while (option != 9);
    //Gabriel Bellagamba End
}


int main(int argc, char *argv[])
{

    int opt1;

    do {

    printf("==SEJA BEM VINDO==\n\n");
    printf("SELECIONE A SUA OPCAO\n\n");
    printf("[1]-COMECAR NOVO JOGO\t");
//    printf("[2]-COMECAR JOGO NOVO\t");
    printf("[9]-SAIR\n");

    scanf("%d", &opt1);

    switch(opt1){

    case 1:
        printf("===Voce selecionou===[%d]\n\n\n",opt1);
        jogar(argc,argv);
        break;
    case 2:
        printf("===Voce selecionou===[%d]\n\n\n",opt1);
        mapa();
        break;
    case 9:
        printf("===Voce selecionou===[%d]\n\n\n",opt1);
        printf("Obrigado por jogar! Ate a proxima\n");

        break;
    default:
        printf("===Voce selecionou===[%d]\n\n\n",opt1);
        printf("Infelizmente essa opcao nao esta disponivel\ntente novamente");
    }
    } while (opt1 != 9);

    return 0;


}

//To compile gplc new_main.pl Game.c
//./new_main
